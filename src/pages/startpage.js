import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import Typed from 'react-typed';

class Intro extends Component {
  render() {
    return (
      <Row id="startpage" className="mx-0">
        <Col
          xs="10"
          sm="10"
          lg="10"
          className="align-self-end ml-4 intro-text-outer"
        >
          <h3 className="intro-text">Hi ich bin Marvin,</h3>
          <Typed
            className="intro-text-typed"
            strings={[
              'ein Planer.',
              'ein Konzepttyp.',
              'ein Macher.',
              'ein Kaffee trinker.',
              'ein Github nerd.',
              'ein Ihre Ideen-umsetzer.'
            ]}
            typeSpeed={75}
            backSpeed={50}
            backDelay={3000}
            cursorChar="|"
            loop
            smartBackspace
          />
          <Col lg="6" className="welcome-text px-0 mb-4">
            Bock auf was neues? Dann bist du hier genau richtig. Schau doch mal
            bei meinen Referenzen vorbei :)
          </Col>
        </Col>
      </Row>
    );
  }
}

export default Intro;
