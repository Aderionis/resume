import React, { Component } from 'react';
import { Button } from 'reactstrap';
import Typed from 'react-typed';
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';

const CATEGORIES = ['Alle', 'Prototypen', 'Digital', 'Banner', 'Social-Media'];
class Gal extends Component {
  constructor(props) {
    super();
    this.photos = props.photos;
    this.state = { currentImage: 0, category: null, photos: this.photos };
    this.closeLightbox = this.closeLightbox.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
  }
  openLightbox(event, obj) {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1
    });
  }
  changeCategory = ev => {
    const category = ev.target.dataset.category;
    let photos = this.photos.filter(photo => {
      if (!category || category === 'Alle') {
        return true;
      } else {
        const categories = Array.isArray(photo.category)
          ? photo.category
          : [photo.category];
        return categories.includes(category);
      }
    });

    this.galleryContainer.classList.add('fade-out');

    const fadeOutHandler = ev =>
      this.setState({ category, photos }, () => {
        this.galleryContainer.classList.remove('fade-out');
        this.galleryContainer.removeEventListener(
          'transitionend',
          fadeOutHandler
        );
      });

    this.galleryContainer.addEventListener('transitionend', fadeOutHandler);
  };
  render() {
    const photos = this.state.photos;
    return (
      <div>
        <div className="d-flex align-self-center">
          <div className="d-inline-flex mb-3">
            <div className="intro-text-typed-gallery">
              <span>Meine - </span>
              <Typed
                strings={['UI/UX', 'Print', 'Digital']}
                typeSpeed={75}
                backSpeed={50}
                backDelay={3000}
                cursorChar=" "
                loop
                smartBackspace
              />
              <span>- arbeiten</span>
            </div>
          </div>
          <div className="d-inline-flex mb-3 ml-auto">
            {CATEGORIES.map(category => (
              <Button
                color="primary"
                className="ml-2 text-uppercase"
                data-category={category}
                key={category}
                onClick={this.changeCategory.bind(this)}
              >
                {category}
              </Button>
            ))}
          </div>
        </div>
        <div class="clearfix" />
        <div
          ref={ref => (this.galleryContainer = ref)}
          className="gallery-container"
        >
          {photos.length < 1 && <p>No items</p>}
          <Gallery
            photos={photos} // I changed here
            onClick={this.openLightbox}
            changeCategory={this.state.category}
            className="mx-auto"
            columns="2"
          />
        </div>
        <Lightbox
          images={photos} // I changed here
          onClose={this.closeLightbox}
          onClickPrev={this.gotoPrevious}
          onClickNext={this.gotoNext}
          currentImage={this.state.currentImage}
          isOpen={this.state.lightboxIsOpen}
          ref={ref => (this.lightboxBackdropfade = ref)}
          className="lightboxBackdropfade"
        />
      </div>
    );
  }
}

export default Gal;
