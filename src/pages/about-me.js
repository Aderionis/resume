import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import ML from '../images/ML-BIG.png';
import Jobs from './jobs.json';
import Typed from 'react-typed';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCoffee,
  faEnvelope,
  faBuilding,
  faCrosshairs,
  faCalendarAlt,
  faLink,
  faHandPeace
} from '@fortawesome/free-solid-svg-icons';

class Aboutme extends Component {
  render() {
    return (
      <Container fluid={true} id="aboutme">
        <Row className="mx-auto justify-content-center">
          <Col lg="8" className="aboutmeview">
            <Row className="personal-aboutme bg-light rounded align-items-center">
              <Col
                lg="7"
                sm={{ size: 12, order: 2, offset: 0 }}
                xs={{ size: 12, order: 2, offset: 0 }}
                md={{ size: 12, order: 2, offset: 0 }}
                className="p-5 personal-aboutme-text"
              >
                <h1 className="green-text">
                  <FontAwesomeIcon icon={faHandPeace} /> Hey ich bin Marvin,
                </h1>
                <h4 className="text-dark">
                  webdesigner aus Leidenschaft & Überzeugung.
                </h4>
                <br />
                <p className="text-dark text-justify">
                  Mein Name ist Marvin Löffler und ich bin 23 Jahre alt.
                  Ungefähr vor 7 Jahren habe ich mein Interesse dem Webdesign
                  gewidmet.
                  <br />
                  <br />
                  Wie man an meinem Lebenslauf sieht, habe ich schon den ein
                  oder anderen Job bestritten. Diese viele Eindrücke haben mir
                  bei meiner Stil und Arbeitsfindung enorm geholfen. Durch die
                  Eindrücke und Erfahrungen kann ich heute gezielt den Mensch
                  hinter dem Produkt zum Produkt beraten und verhelfen.
                  <br />
                  <br />
                  In meinem letzten Job habe ich als Online Marketing Manager &
                  Webdesigner mit dem Schwerpunkt SEO, SEM, Usability und
                  Website Redesign bei der Firma Ankermann Computer e.K in
                  Teningen gewerkelt.
                  <br />
                  <br />
                  Sie sind überzeugt? Dann sollten wir uns auf ein Kaffee
                  treffen. Sie können mich über{' '}
                  <FontAwesomeIcon icon={faCoffee} /> 0176 6422 430 7 oder{' '}
                  <FontAwesomeIcon icon={faEnvelope} /> marvin@aderion.de
                  erreichen.
                </p>
              </Col>
              <Col
                lg="5"
                sm={{ size: 12, order: 1, offset: 0 }}
                xs={{ size: 12, order: 1, offset: 0 }}
                md={{ size: 12, order: 1, offset: 0 }}
                className="p-5 justify-content-center d-none d-sm-none d-md-none d-lg-block"
              >
                <img
                  src={ML}
                  className="img-fluid ml-image w-100"
                  alt="Marvin Löffler"
                />
              </Col>
            </Row>
            <Row className="mt-4">
              <div className="d-flex align-self-center">
                <div className="d-inline-flex mb-3">
                  <div className="intro-text-typed-gallery">
                    <span>Mein </span>
                    <Typed
                      strings={['Lebenslauf', 'Werdegang']}
                      typeSpeed={75}
                      backSpeed={50}
                      backDelay={3000}
                      cursorChar=" "
                      loop
                      smartBackspace
                    />
                  </div>
                </div>
              </div>
              {Jobs.map(item => (
                <div className="list-group w-100 mb-5">
                  <a
                    href={item.website}
                    target="_blank"
                    className="list-group-item list-group-item-action flex-column  align-items-start p-5 jobcard"
                  >
                    <div className="d-flex flex-wrap w-100 justify-content-between mb-3 mt-3">
                      <h5 className="align-self-center jobtitle">
                        <FontAwesomeIcon icon={faBuilding} /> {item.title}
                      </h5>
                      <h6 className="align-self-center jobposition">
                        <FontAwesomeIcon icon={faCrosshairs} /> {item.position}
                      </h6>
                    </div>
                    <p className="mb-3 text-justify">{item.description}</p>
                    <div className="d-flex flex-wrap w-100 justify-content-between mb-3">
                      <small className="jobemployment">
                        <FontAwesomeIcon icon={faCalendarAlt} />{' '}
                        {item.employment} vom {item.start} bis {item.end}
                      </small>
                      <small className="jobwebsite">
                        <FontAwesomeIcon icon={faLink} /> Website:{' '}
                        {item.website}
                      </small>
                    </div>
                  </a>
                </div>
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Aboutme;
