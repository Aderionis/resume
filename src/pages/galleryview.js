import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import photos from './photos.json';
import Gal from './gallery';

class Content extends Component {
  render() {
    return (
      <Container fluid="true" id="gallery">
        <Row className="mx-auto justify-content-center">
          <Col lg="8" className="galleryview">
            <Gal photos={photos} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Content;
