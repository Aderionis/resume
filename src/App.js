import React, { Component } from 'react';
import './App.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee, faImages } from '@fortawesome/free-solid-svg-icons';

// Logo
import logo from './images/aderion-logo.svg';
// Logo

// Pages
import startpage from './pages/startpage';
import referenzen from './pages/galleryview';
import aboutme from './pages/about-me';
// Pages

class App extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="dark" dark expand="sm" fixed="top">
          <NavbarBrand href="/" className="ml-4 mr-4 pt-3 pb-3">
            <img src={logo} className="logo" alt="logo" />
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar className="text-center">
            <Nav navbar className="ml-auto">
              <NavItem>
                <NavLink className="nav-link" href="/gallery">
                  <FontAwesomeIcon icon={faImages} /> Referenzen
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="nav-link" href="/about">
                  <FontAwesomeIcon icon={faCoffee} /> About me
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <Router>
          <Switch>
            <Route exact path="/" component={startpage} />
            <Route exact path="/gallery" component={referenzen} />
            <Route exact path="/about" component={aboutme} />
          </Switch>
        </Router>
      </div>
    );
  }
}
export default App;
